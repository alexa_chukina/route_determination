let locationJSON = {};
let locationParams = {};
let distanceMatrix = new Array();
let map;
let readyCounter = 0;
// Объект соответсвия типа локации и метки на карте
let presets = {
  'Sights & Landmarks' : 'islands#blueIcon',
  'Museums & Libraries' : 'islands#blueIcon',
  'Restaurant' : 'islands#redIcon',
}

// Событие окончания заполнения матрицы расстояний между точками
var distanceReadyEvent = new Event('distanceReady');
document.addEventListener('distanceReady', distanceSort, false);
// Событие готовности сортировки исходного массива в зависимости от минимального расстояния до следующей точки
var distanceSortReadyEvent = new Event('distanceSortReady');
document.addEventListener('distanceSortReady', init, false);

// Запуск функции чтения файла с исходными данными
readJSON('Kazan.json');

function readJSON(file) {
  // Формирование http запроса
  const xhr = window.XMLHttpRequest ? ( new XMLHttpRequest() ) : ( new ActiveXObject("Microsoft.XMLHTTP") );
  xhr.open("GET", file, true);
  xhr.onreadystatechange = function () {
    if(xhr.readyState === 4) {
      // При удачном выполнении запроса
      if(xhr.status === 200 || xhr.status == 0) {
        // Ответ преображуем в json формат
        locationJSON = JSON.parse(xhr.responseText);
        // Создаем вложенный массив для матрицы расстояний
        for (var i=0; i<locationJSON.length; i++) { distanceMatrix[i]=new Array(locationJSON.length); }
        // Получение координат центра карты и минимальных/максимальных широт/долгот для поределения области карты
        getLocationParams(locationJSON);
        // Сортировка исходного объекта по популярности на сервисе foursquare
        sortByKey('foursquare_rating');
        ymaps.ready(getDistance);
      }
    }
  }
  xhr.send(null);
}

function init() {
  // Создание экземпляра карты и его привязка к контейнеру с
  // заданным id ("map").
  map = new ymaps.Map('map',
    {
      // При инициализации карты обязательно нужно указать
      // её центр и коэффициент масштабирования.
      center: [locationParams.meanLat, locationParams.meanLng],
      zoom: 10,
      controls: [], // Убрать служебные кнопки yandex.map
      bounds: [[ (locationParams.maxLat + 0.01), (locationParams.minLng - 0.01) ],[ (locationParams.minLat - 0.01), (locationParams.maxLng + 0.01) ]] // Задаются в географических координатах самой юго-восточной и самой северо-западной точек видимой области.
    },
    {
      suppressMapOpenBlock: true,
      searchControlProvider: 'yandex#search'
    }
  );

  let locations = [];
  for( key in locationJSON ) {
    locations[key] = [ locationJSON[key].lat, locationJSON[key].lng ];
  }

  let multiRoute = new ymaps.multiRouter.MultiRoute(
    {
      referencePoints: locations,
      params: {
        // Тип маршрутизации
        routingMode: 'pedestrian'
      }
    },
    {
      // Скрывать путевые и транзитные точки.
      wayPointVisible: false,
      viaPointVisible: false,
      activeRouteAutoSelection: true,
      // Внешний вид линии пешеходного маршрута.
      routeActivePedestrianSegmentStrokeStyle: "solid",
      // Отключить собственные маркеры маршрута
      routeActiveMarkerVisible: false,
      // Отключить всплывание балуна маршрута
      routeOpenBalloonOnClick: false
    }
  );
  map.geoObjects.add(multiRoute);

  for( key in locationJSON ) {
    map.geoObjects.add(
      new ymaps.GeoObject({
        // Описание геометрии.
        geometry: {
          type: "Point",
          coordinates: [locationJSON[key].lat, locationJSON[key].lng]
        },
        // Свойства.
        properties: {
          // Контент метки.
          iconContent: (+key + 1),
          balloonContent: createBallon( key ), // Вызов функции формирующей содержимое балона
        }
      }, {
        // Опции.
        // Иконка метки будет растягиваться под размер ее содержимого.
        preset: checkMapPreset( locationJSON[key].categories ), // Функция определения типа пресета. Красные - рестораны, достопримечательности - синие
      })
    );
  }
}

/** Опраделение параметров положения карты. Минимальные/максимальные значения широт/долгот по исходным данным, а также определение положения центра карты.*/
function getLocationParams() {

  let minMaxLat = minMax ( locationJSON, 'lat' );
  let minMaxLng = minMax ( locationJSON, 'lng' );

  locationParams = {
    'meanLat' : mean(locationJSON, 'lat'),
    'maxLat' : minMaxLat.max,
    'minLat' : minMaxLat.min,
    'meanLng' : mean(locationJSON, 'lng'),
    'maxLng' : minMaxLng.max,
    'minLng' : minMaxLng.min
  }

  return locationParams;
}

/** Нахождение типа маркера точки в зависимости от типа объекта для отображения. Входные параметры:
* @param string categories - категория объекта
*/
function checkMapPreset( categories ) {
  for( cat in categories ) {
    if( presets[ categories[cat] ] ) {
      return presets[ categories[cat] ];
    }
  }
  return 'islands#grayIcon';
}

/** Нахождение среднего значения указанного праметра в объекте. Входные параметры:
* @param object obj - объект
* @param string key - параметр для которого нужно найти min/max значения
*/
function mean ( obj, key ) {
  let lenObj = obj.length;
  let sum = 0;
  obj.map( function( row ){ sum += row[key]; } );
  return sum / lenObj;
}

/** Нахождение минимального/максимального значения. Входные параметры:
* @param object obj - объект
* @param string key - параметр для которого нужно найти min/max значения
*/
function minMax ( obj, key ) {
  let min = 361;
  let max = -1;

  obj.map( function( row ) {
    if( row[key] < min ){ min = row[key]; }
    if( row[key] > max ){ max = row[key]; }
  });

  return { min, max };
}

/** Сортировка исходного объекта (locationJSON) от большего к меньшему по указанному ключу. Входные параметры:
* @param string key - параметр объекта по значению в котором будет происходить сортировка
*/
function sortByKey( key ) {
  locationJSON.sort(function(a, b){
    return a[key]-b[key];
  });
  return true;
}

/** Получение растояния по прямой из точки с координатами lat1, lon1 до точки lat2,lon2. Входные параметры:
* @param number lat1 - широта 1 точки
* @param number lon1 - долгота 1 точки
* @param number lat2 - широта 2 точки
* @param number lon2 - долгота 2 точки
*/
function distance( lat1, lon1, lat2, lon2 ) {
  var radius = 6371302;
  lat1 = lat1*Math.PI/180.0;
  lat2 = lat2*Math.PI/180.0;
  lon1 = lon1*Math.PI/180.0;
  lon2 = lon2*Math.PI/180.0;
  var delta = lon2 - lon1;
  var cl1 = Math.cos(lat1);
  var cl2 = Math.cos(lat2);
  var sl1 = Math.sin(lat1);
  var sl2 = Math.sin(lat2);
  var cdelta = Math.cos(delta);
  var sdelta = Math.sin(delta);
  var y = Math.sqrt(Math.pow(cl2*sdelta,2) + Math.pow(cl1*sl2-sl1*cl2*cdelta,2));
  var x = sl1*sl2 + cl1*cl2*cdelta;
  var ad = Math.atan2(y, x);
  var dist = ad*radius;
  return dist;
}

/** Заполнение матрицы дистанций для всех точек маршрута. */
function getDistance() {
  for( row in locationJSON ) {
    for( key in locationJSON ) {
      distanceMatrix[row][key] = distance(locationJSON[row].lat, locationJSON[row].lng, locationJSON[key].lat, locationJSON[key].lng);
      readyCounter++;
      if( readyCounter == (locationJSON.length*locationJSON.length) ) {
        // Вызываем событие
        document.dispatchEvent(distanceReadyEvent);
      }
    }
  }
}

/** Сортировка исходного объекта по дистанциям между точками с целью выстроить наиболее кароткий маршрут. */
function distanceSort() {

  // Создаем массив с индексами точек [0,1,2...15]
  let array = [];
  for(let i=0; i<locationJSON.length; i++) {
    array.push(i);
  }

  // Перебираем все индексы в массиве, удаляем использованные,
  // до тех пор пока не кончатся все индексы
  let keyNow = array[0];
  let keyPrev = -1;
  while( array.length > 0 ) {
    let idx = array.indexOf(keyNow);
    array.splice(idx, 1);
    let minDist = 150000;
    let minKey = keyNow;
    for( i in array ) {
      let dist = distanceMatrix[keyNow][array[i]];
      if(dist < minDist) {
        minDist = dist;
        minKey = array[i];
      }
    }
    locationJSON[keyNow]['nearest_point'] = minKey;
    locationJSON[keyNow]['distance_to_next_point'] = minDist;
    if( keyPrev > -1 ) {
      locationJSON[keyNow]['distance_from_start'] = locationJSON[keyPrev]['distance_from_start'] + locationJSON[keyPrev]['distance_to_next_point'];
    }
    else {
      locationJSON[keyNow]['distance_from_start'] = 0.0;
    }
    keyPrev = keyNow;
    keyNow = minKey;
  }

  // Сортировка по расстоянию от начала маршрута
  let checkReadySort = sortByKey('distance_from_start');
  if ( checkReadySort ) {
    document.dispatchEvent(distanceSortReadyEvent);
  }
}

/** Заполнение полей матрицы дистанций между рассматриваемыми точками. Входные параметры:
* @param object route - ответ api yandex map ()
*/
function updateDistanceMatrix( route ) {
  let keys = {};
  for( key in locationJSON ) {
    if( locationJSON[key].lat == route.requestPoints[0][0] && locationJSON[key].lng == route.requestPoints[0][1] ) { keys['start'] = key; }
    if( locationJSON[key].lat == route.requestPoints[1][0] && locationJSON[key].lng == route.requestPoints[1][1] ) { keys['stop'] = key; }
  }
  distanceMatrix[keys.start][keys.stop] = route.getLength();
  return;
}

/** Создание html кода балуна точки на карте. Балун должен содержать название, изображение, тип объекта и рейтинг в соцсетях. Входные параметры:
* @param string key - номер рассматриваемого точки маршрута
*/
function createBallon( key ) {
  let row =  locationJSON[key].title.length != 0 ? '<div class="location-title">'+ locationJSON[key].title +'</div>' : '<div class="location-title">Неизвестно</div>';
  if( locationJSON[key].title.length != 0 ) {
    row += '<img class="location-photo" src="/img/attractions/'+locationJSON[key].title+'.jpg" alt="Упс! Что-то пошло не так..."/>';
  }

  if( locationJSON[key].categories.length != 0 ) {
    row += '<div class="categories">';
    for( cat in locationJSON[key].categories ) {
      row += locationJSON[key].categories[cat] + ( locationJSON[key].categories.length == 1 && locationJSON[key].categories.length != cat ? '':', ' );
    }
    row += '</div>';
  } else {
    row += '<div class="categories">Категория не определена</div>';
  }

  let social = '<div class="social-row">'
  if( locationJSON[key].facebook_rating.length != 0 ) {
    social += '<div class="rating"><img class="icon" src="/img/facebook.png" alt="Упс! Что-то пошло не так..."/>';
    social += '<strong>'+ locationJSON[key].facebook_rating +'</strong></div>';
  }
  if( locationJSON[key].tripAdvisor_rating.length != 0 ) {
    social += '<div class="rating"><img class="icon" src="/img/trip_advisor.png" alt="Упс! Что-то пошло не так..."/>';
    social += '<strong>'+ locationJSON[key].tripAdvisor_rating +'</strong></div>';
  }
  if ( locationJSON[key].foursquare_rating.length != 0 ) {
    social += '<div class="rating"><img class="icon" src="/img/foursquare.png" alt="Упс! Что-то пошло не так..."/>';
    social += '<strong>'+ locationJSON[key].foursquare_rating +'</strong></div>';
  }
  social += '</div>';

  row += social;
  return row;
}
